package com.example.datagenerator;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

public interface IGenerator {
	
	
	public abstract void generate(Context context, LatLng coord);
	

}