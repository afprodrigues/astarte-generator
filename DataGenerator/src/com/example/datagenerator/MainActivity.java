package com.example.datagenerator;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.example.datagenerator.Generator.GenType;
import com.google.android.gms.maps.model.LatLng;


public class MainActivity extends ActionBarActivity {

	private final String FILE_PATH = "/storage/emulated/0/Download/configurationCentral.xml";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void generate_data_from_file(View v) throws IOException {
		File xmlFile = new File(FILE_PATH);

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		Document doc = null;

		try {
			dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(xmlFile);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			new AlertDialog.Builder(this)
					.setMessage("It was not possible to read the file. Please try again.")
					.setTitle("Error").setCancelable(true).show();
		}
		if (doc != null) {

			Node nNode = doc.getDocumentElement();

			String generator_type_string = null;
			String lat_String = null;
			String lng_String = null;
			double lat = 200;
			double lng = 200;
			int maxDist = 0;
			int num_ger = 0;
			int interval_ger = 0;
			int minP = 0;
			int maxP = 0;
			int minPoints = 0;
			int maxPoints = 0;
			int minScreen = 0;
			int maxScreen = 0;
			int minSteps = 0;
			int maxSteps = 0;
			LatLng coord = null;

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;

				generator_type_string = eElement
						.getElementsByTagName("gen_type").item(0)
						.getTextContent();

				Generator.GenType generator_type = Generator.GenType
						.valueOf(generator_type_string);

				lat_String = eElement.getElementsByTagName("lat").item(0)
						.getTextContent();
				lng_String = eElement.getElementsByTagName("lng").item(0)
						.getTextContent();
				
				boolean validCoords = true;
				if (!lat_String.equals("") && !lng_String.equals("")) {
					lat = Double.parseDouble(lat_String);
					lng = Double.parseDouble(lng_String);
					validCoords = validCoords(lat, lng);
					if (validCoords)
						coord = new LatLng(lat, lng);
					else
						validCoords = false;
				}
				
				if(validCoords) {

					maxDist = Integer.parseInt(eElement
							.getElementsByTagName("max_dist").item(0)
							.getTextContent());
					num_ger = Integer.parseInt(eElement
							.getElementsByTagName("num_ger").item(0)
							.getTextContent());
					interval_ger = Integer.parseInt(eElement
							.getElementsByTagName("int_ger").item(0)
							.getTextContent());
					minP = Integer.parseInt(eElement
							.getElementsByTagName("min_victims").item(0)
							.getTextContent());
					maxP = Integer.parseInt(eElement
							.getElementsByTagName("max_victims").item(0)
							.getTextContent());
					minPoints = Integer.parseInt(eElement
							.getElementsByTagName("min_points").item(0)
							.getTextContent());
					maxPoints = Integer.parseInt(eElement
							.getElementsByTagName("max_points").item(0)
							.getTextContent());
					minScreen = Integer.parseInt(eElement
							.getElementsByTagName("min_screen").item(0)
							.getTextContent());
					maxScreen = Integer.parseInt(eElement
							.getElementsByTagName("max_screen").item(0)
							.getTextContent());
					minSteps = Integer.parseInt(eElement
							.getElementsByTagName("min_steps").item(0)
							.getTextContent());
					maxSteps = Integer.parseInt(eElement
							.getElementsByTagName("max_steps").item(0)
							.getTextContent());

					if(validData (minP, maxP, minPoints, maxPoints, minScreen,
							maxScreen, minSteps, maxSteps)) {

						IGenerator generator = create_generator(generator_type,
								maxDist, num_ger, interval_ger, minP, maxP, minPoints,
								maxPoints, minScreen, maxScreen, minSteps, maxSteps);

						generator.generate(this, coord);
						
						new AlertDialog.Builder(this)
						.setMessage("Data has been successfully generated!")
						.setTitle("Success").setCancelable(true).show();
					}
				}		
			}
			else {
				new AlertDialog.Builder(this)
				.setMessage("It was not possible to read the file. Please try again.")
				.setTitle("Error").setCancelable(true).show();
			}
		}
		else {
			new AlertDialog.Builder(this)
			.setMessage("It was not possible to read the file. Please try again.")
			.setTitle("Error").setCancelable(true).show();
		}
	}
	
	/**
	 * Valida as coordenadas indicadas pelo utilizador
	 * @param lat - a latitude indicada
	 * @param lng - a longitude indicada
	 * @return - true se as coordenadas sao validas, false cc
	 */
	private boolean validCoords(double lat, double lng) {
		if (lat < -90 || lat < -90) {
			new AlertDialog.Builder(this)
			.setMessage("Latitude must be between -85 and 85. Exiting system...")
			.setTitle("Error").setCancelable(true).show();
			return false;
		}
		else {
			if (lng < -180 || lng < -180) {
				new AlertDialog.Builder(this)
				.setMessage("Longitude must be between -180 and 180. Exiting system...")
				.setTitle("Error").setCancelable(true).show();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Valida os dados indicados pelo utilizador
	 * @param minP - minimo numero de vitimas
	 * @param maxP - maximo numero de vitimas
	 * @param minPoints - minimo numero de pontos por vitima
	 * @param maxPoints - maximo numero de pontos po vitima
	 * @param minScreen - maximo numero de toques ecra
	 * @param maxScreen - maximo numero de toques ecra
	 * @param minSteps - minimo numero de passos
	 * @param maxSteps - maximo numero de passos
	 * @return - true se os dados sao validos, false cc
	 */
	private boolean validData(int minP, int maxP, int minPoints, int maxPoints, int minScreen,
			int maxScreen, int minSteps, int maxSteps) {

		boolean valid = true;
		if (maxP < minP) {
			valid = false;
			new AlertDialog.Builder(this)
			.setMessage("max_victims must be greater than or equal to min_victims. Exiting system...")
			.setTitle("Error").setCancelable(true).show();
		}
		else {
			if (maxPoints < minPoints) {
				valid = false;
				new AlertDialog.Builder(this)
				.setMessage("max_points must be greater than or equal to min_points. Exiting system...")
				.setTitle("Error").setCancelable(true).show();
			}
			else {
				if (maxScreen < minScreen) {
					valid = false;
					new AlertDialog.Builder(this)
					.setMessage("max_screen must be greater than or equal to min_screen. Exiting system...")
					.setTitle("Error").setCancelable(true).show();
				}
				else {
					if (maxSteps < minSteps) {
						valid = false;
						new AlertDialog.Builder(this)
						.setMessage("max_steps must be greater than or equal to min_steps. Exiting system...")
						.setTitle("Error").setCancelable(true).show();
					}
				}
			}
		}
		return valid;
	}

	private IGenerator create_generator(GenType generator_type, int maxDist,
			int num_ger, int interval, int minP, int maxP, int minPoints,
			int maxPoints, int minScreen, int maxScreen, int minSteps,
			int maxSteps) {
		IGenerator generator;
		switch (generator_type) {
		case CENTRAL:
			generator = new GeneratorCentral(this, maxDist, num_ger, interval,
					minP, maxP, minPoints, maxPoints, minScreen, maxScreen,
					minSteps, maxSteps);
			break;
		case FIELD:
			generator = new GeneratorField(this, maxDist, num_ger, interval,
					minP, maxP, minPoints, maxPoints, minScreen, maxScreen,
					minSteps, maxSteps);
			break;
		default:
			generator = null;
			break;
		}
		return generator;
	}

	// public void generate_data(View v) {
	// EditText latField = (EditText) findViewById(R.id.EditText1);
	// String lat_String = latField.getText().toString();
	// double lat = 0;
	//
	// EditText lngField = (EditText) findViewById(R.id.EditText2);
	// String lng_String = lngField.getText().toString();
	// double lng = 0;
	//
	// if (lat_String == null || lng_String == null) {
	//
	// Location location = locationManager.getLastKnownLocation(provider);
	// // Initialize the location fields
	// if (location == null) {
	// System.out.println("Provider " + provider
	// + " has been selected.");
	// // onLocationChanged(location);
	// } else {
	// lat = location.getLatitude();
	// lng = location.getLongitude();
	// }
	//
	// } else {
	// lat = Double.parseDouble(lat_String);
	// lng = Double.parseDouble(lng_String);
	// }
	//
	// LatLng coord = new LatLng(lat, lng);
	//
	// EditText maxDistField = (EditText) findViewById(R.id.EditText3);
	// int maxDist = Integer.parseInt(maxDistField.getText().toString());
	//
	// EditText numGerField = (EditText) findViewById(R.id.EditText4);
	// int num_ger = Integer.parseInt(numGerField.getText().toString());
	//
	// EditText intGerField = (EditText) findViewById(R.id.EditTextIntGer);
	// int int_ger = Integer.parseInt(intGerField.getText().toString());
	//
	// EditText minPField = (EditText) findViewById(R.id.EditText5);
	// int minP = Integer.parseInt(minPField.getText().toString());
	//
	// EditText maxPField = (EditText) findViewById(R.id.EditText6);
	// int maxP = Integer.parseInt(maxPField.getText().toString());
	//
	// EditText minPointsField = (EditText) findViewById(R.id.EditText7);
	// int minPoints = Integer.parseInt(minPointsField.getText().toString());
	//
	// EditText maxPointsField = (EditText) findViewById(R.id.EditText8);
	// int maxPoints = Integer.parseInt(maxPointsField.getText().toString());
	//
	// EditText minScreenField = (EditText) findViewById(R.id.EditText9);
	// int minScreen = Integer.parseInt(minScreenField.getText().toString());
	//
	// EditText maxScreenField = (EditText) findViewById(R.id.EditText10);
	// int maxScreen = Integer.parseInt(maxScreenField.getText().toString());
	//
	// EditText minStepsField = (EditText) findViewById(R.id.EditText11);
	// int minSteps = Integer.parseInt(minStepsField.getText().toString());
	//
	// EditText maxStepsField = (EditText) findViewById(R.id.EditText12);
	// int maxSteps = Integer.parseInt(maxStepsField.getText().toString());
	//
	// IGenerator generator = new GeneratorField(this, maxDist, num_ger,
	// int_ger, minP, maxP, minPoints, maxPoints, minScreen,
	// maxScreen, minSteps, maxSteps);
	// generator.generate(v, this, coord);
	// }

}
